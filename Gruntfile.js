module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        sass: {
            target: {
                options: {
                    style: 'expanded',
                },
                files: { 
                    'prod/css/style.css': 'dev/sass/style.scss'
                }
            }
        },
        cssmin: {
            target: {
                options: {
                    position: 'top',
                    banner: '/**\n*\tDate created: <%= grunt.template.today("yyyy-mm-dd") %>' +
                            '\n*\tAuthor: <% pkg.author.name %>\n*/' 
                },
                files: [{
                    expand: true,
                    cwd: 'prod/css/',
                    src: ['*.css', '!*.min.css'],
                    dest: 'prod/css/',
                    ext: '.css'
                }]
            }
        },
        jshint: {
            target: {
                files: {
                    src: ['dev/js/custom.js']
                },
                options: {
                    curly: true,
                    eqeqeq: true
                }
            }
        },
        concat: {
            target: {
                libs: {
                    src: [
                            'bower/path/to/jquery.js',
                            'bower/path/to/modernizr.js',
                            'bower/path/to/otherfiles.js'
                        ],
                    dest: 'prod/js/lib.js'
                }
            }
        },
        uglify: {
            target: {
                options: {
                    mangle: false,
                    banner: '/**\n*\tDate created: <%= grunt.template.today("yyyy-mm-dd") %>' +
                            '\n*\tAuthor: <% pkg.author.name %>\n*/' 
                },
                files: {
                    'prod/js/lib.js': 'prod/js/lib.js',
                    'prod/js/custom.js': 'dev/js/custom.js'
                }
            }
        },
        watch:{
            target: {
                files: ['dev/sass/*.scss', 'dev/js/*.js'],
                tasks: ['sass', 'cssmin', 'jshint', 'concat', 'uglify'],
                options: {
                    livereload: false
                }
            }
        },
        exec: {
            jquery: {
                cmd: 'bower install jquery',
                stderr: false
            },
            modernizr: {
                cmd: 'bower install modernizr-min',
                stderr: false
            },
            bootstrap: {
                cmd: 'bower install git://github.com/twbs/bootstrap-sass.git',
                stderr: false
            },
            normalize: {
                cmd: 'bower install git://github.com/hail2u/normalize.scss.git',
                stderr: false
            },
            grunt: {
                cmd: 'sudo npm install grunt --save-dev',
                sdterr: false
            },
            watch: {
                cmd: 'sudo npm install grunt-contrib-watch --save-dev',
                sdterr: false
            },
            sass: {
                cmd: 'sudo npm install grunt-contrib-sass --save-dev',
                sdterr: false
            },
            cssmin: {
                cmd: 'sudo npm install grunt-contrib-cssmin --save-dev',
                sdterr: false
            },
            jshint: {
                cmd: 'sudo npm install grunt-contrib-jshint --save-dev',
                sdterr: false
            },
            concat: {
                cmd: 'sudo npm install grunt-contrib-concat --save-dev',
                sdterr: false
            },
            uglify: {
                cmd: 'sudo npm install grunt-contrib-uglify --save-dev',
                sdterr: false
            },
            require: {
                cmd: 'sudo npm install requirejs',
                stderr: false
            },
            update:{
                cmd: 'sudo npm update grunt-*',
                stderr: false
            }
        }
    });

    /*grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');*/
    grunt.loadNpmTasks('grunt-exec');

    var Template = {
        buildProjectTemplate: function() {
            var dirs = [
                    'dev',
                    'dev/js',
                    'dev/sass',
                    'dev/sass/partials',
                    'dev/sass/pages',
                    'prod',
                    'prod/css',
                    'prod/fonts',
                    'prod/images',
                    'prod/js'
                ],
                files = [
                    'style.scss',
                    'partial_extends.scss',
                    'partial_mixin.scss',
                    'partial_fonts.scss',
                    'partial_variables.scss',
                    'partial_mediaquery.scss',
                    'partial_bootstrapoverrides.scss',
                    'page_home.scss',
                    'page_aboutus.scss',
                    'page_contactus.scss',
                    'page_disclaimer.scss',
                    'page_termsandconditions.scss',
                    'custom.js',
                    'index.html',
                    '.bowerrc',
                    'CNAME'
                ],
                dirsLen = dirs.length - 1,
                filesLen = files.length - 1,
                counter;

            /**
             * create directories
             */
            for (counter = 0; counter <= dirsLen; counter++) {

                grunt.file.mkdir(dirs[counter]);
            }

            /**
             * create files
             */
            for (counter = 0; counter <= filesLen; counter++) {

                /**
                 *   string has .scss
                 */
                if (files[counter].indexOf('.scss') !== -1) {

                    /**
                     *   string has style
                     */
                    if (!(files[counter].indexOf('style'))) {

                        grunt.file.write(
                            'dev/sass/' + files[counter],
                            '@import \'path/to/your/bootstrap.scss\'\,\n\t\t\'path/to/your/normalize.scss\'\,\n\t\t\'variables\'\,\n\t\t\'fonts\'\,\n\t\t\'extends\'\,\n\t\t\'mixins\'\,\n\t\t\'bootstrapoverrides\'\,\n\t\t\n\n*{padding: 0px; margin: 0px;}\nbody{margin: 0px 0px;}'
                        );
                    }
                    /**
                     *   string has partial
                     */
                    else if (!(files[counter].indexOf('partial'))) {

                        grunt.file.write(
                            'dev/sass/partials/' + files[counter].replace('partial', '')
                        );
                    }
                    /**
                     *   string has page
                     */
                    else {
                        grunt.file.write(
                            'dev/sass/pages/' + files[counter].replace('page', '')
                        );
                    }
                }

                /**
                 *   string has .js
                 */
                if (files[counter].indexOf('.js') !== -1) {
                    grunt.file.write(
                        'dev/js/' + files[counter]
                    );
                }

                /**
                 *   string has .html
                 */
                if (files[counter].indexOf('.html') !== -1) {
                    grunt.file.write(
                        files[counter],
                        '<!DOCTYPE html>\n<!--[if lt IE 7 ]><html lang="en" class="no-js ie6"><![endif]-->\n<!--[if IE 7 ]><html lang="en" class="no-js ie7"><![endif]-->\n<!--[if IE 8 ]><html lang="en" class="no-js ie8"><![endif]-->\n<!--[if IE 9 ]><html lang="en" class="no-js ie9"><![endif]-->\n<!--[if (gt IE 9)|!(IE)]><!-->\n<html lang="en" class="no-js">\n<!--<![endif]-->\n<head>\n\t<meta charset="utf-8">\n\t<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">\n\t<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">\n\t<meta name="robots" content="noindex, nofollow">\n\t<meta name="content" description="">\n\t<meta name="keywords" description="">\n\t<title>Page Title</title>\n\t<link rel="icon" type="image/png" href="http://example.com/myicon.png">\n\t<link href="prod/css/style.min.css" rel="stylesheet">\n\t<!-- HTML5 shim and Respond.js forssss IE8 support of HTML5 elements and media queries -->\n\t<!-- WARNING: Respond.js doesn\'t work if you view the page via file:// -->\n\t<!--[if lt IE 9]>\n\t\t<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>\n\t\t<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>\n\t<![endif]-->\n</head>\n<body>\n\n\t<header class="">\n\n\t</header>\n\t<section class="">\n\n\t</section>\n\t<section class="">\n\n\t</section>\n\t<footer class="">\n\n\t</footer>\n\n\t<script type="text/javascript" src="prod/js/lib.min.js"></script>\n\t<script type="text/javascript" src="prod/js/custom.js"></script>\n</body>\n</html>'
                    );
                }

                /**
                 *   string has CNAME
                 */
                if (files[counter].indexOf('bowerrc') !== -1) {
                    grunt.file.write(
                        files[counter],
                        '{"directory" : "bower"}'
                    );
                }

                /**
                 *   string has CNAME
                 */
                if (files[counter].indexOf('CNAME') !== -1) {
                    grunt.file.write(
                        files[counter],
                        'cname.test.com'
                    );
                }
            }
        },
        executeBower: function() {
            
            grunt.task.run('exec:jquery');
            grunt.task.run('exec:modernizr');
            grunt.task.run('exec:bootstrap');
            grunt.task.run('exec:normalize');
            grunt.task.run('exec:watch');
            grunt.task.run('exec:sass');
            grunt.task.run('exec:cssmin');
            grunt.task.run('exec:jshint');
            grunt.task.run('exec:concat');
            grunt.task.run('exec:uglify');
            grunt.task.run('exec:require');

        },
        executeUpdate: function(){

            grunt.task.run('exec:update');
        }
    };

    grunt.task.registerTask('files', function() {

        Template.buildProjectTemplate();
        Template.executeBower();
    });

    grunt.task.registerTask('depUpdate', function() {

        Template.executeUpdate();
    });

    grunt.task.registerTask('default', ['watch']);
};